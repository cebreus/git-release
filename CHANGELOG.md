## 2019-05-22, Version [1.1.0](https://bitbucket.org/onio-it-team/git-release/compare/1.1.1..1.0.1)

### Notable changes

-   base structure change
-   run script witch sematic version parameter
-   user notable changes

### Commits

-   [ee3b22d](https://bitbucket.org/onio-it-team/git-release/commits/ee3b22d33b8ad6d51ab32d113f57e3514e35f0b3) - feat: add readme.md
-   [7607d6d](https://bitbucket.org/onio-it-team/git-release/commits/7607d6d70fd2e3f097e8326cb5a765fd76d2062d) - fix: first tag gen
-   [57d89b7](https://bitbucket.org/onio-it-team/git-release/commits/57d89b723cee7ace72ec2aceb29e6f36bafd12c7) - feat: last commit :D
-   [44ec50c](https://bitbucket.org/onio-it-team/git-release/commits/44ec50c4ce4359ea07095d0fb61e08da3a7f5214) - fix: cd ../
-   [2682043](https://bitbucket.org/onio-it-team/git-release/commits/26820435bd98d1d03578652a23394eefa667b512) - fix: script running
-   [ced0733](https://bitbucket.org/onio-it-team/git-release/commits/ced0733e6c704c8bfedacc66e12d94b0396d6f3a) - fix: current folder validation
-   [301ad47](https://bitbucket.org/onio-it-team/git-release/commits/301ad474ccc9312380a5a913a964dd81f2ed2f05) - Merge tag '1.0.1' into develop

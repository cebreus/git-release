# Git release

> Shell scripts for release process and changelog generating.

## DESCRIPTION

The application contains many shell script and has only one dependency on "git".
You will use only `run.sh` script. Other scripts are only helpers.

## Development setup

Run in your new repository.

### if you use firstly

```sh
git clone [your-wtf-repository]
cd [your-wtf-repository]
# git flow init
git flow init -fd
# clone to the new git repository
git submodule add git@bitbucket.org:onio-it-team/git-release.git
# git submodule add https://<YOUR-BB-USER-NAME>@bitbucket.org/onio-it-team/git-release.git
```

### if you have alredy used in repository

```sh
git clone [your-wtf-repository]
cd [your-wtf-repository]
# git flow init
git flow init -fd
# update all submodules dependencies
cd git-release
git submodule update --init --recursive
```

## Usage example

-   `cd git-release && ./run.sh patch`
-   `cd git-release && ./run.sh minor`
-   `cd git-release && ./run.sh major`

## Knows issues

### '\\r': command not found in Bash

Remove trailing \\r character that causes this error:

```bash
sed -i 's/\\r$//' filename
```

## Support

You can create ticket on [Trello](https://trello.com/b/kDfZM8gY/support-do%C4%8Dasn%C3%A9).

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [CHANGELOG.md](https://bitbucket.org/onio-it-team/git-release/src/develop/CHANGELOG.md)

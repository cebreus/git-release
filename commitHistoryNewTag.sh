#!/bin/bash
source './parseRepository.sh'
source './commitHistoryOutput.sh'

function commitHistoryNewTag() {
    if [ -z "$1" ]; then
        echo "Error: First parameter is empty. Please send tag parameter"
        exit 2
    fi
    
    local tag=$1
    outputMsgs=()
    local commits=$(git log --pretty=tformat:'%H')
    notableChangesSource="./git-release/customChange.md"
    
    ## notable changes
    notableChangesInput $notableChangesSource
    
    outputMsgs+=("## $(date +'%Y-%m-%d'), version $tag")
    outputMsgs+=("")
    #outputMsgs+=("### Notable changes");
    
    while IFS= read -r line; do
        outputMsgs+=("$line")
    done <$notableChangesSource
    
    outputMsgs+=("")
    outputMsgs+=("### Commits")
    outputMsgs+=("")
    
    if [ -f $notableChangesSource ]; then
        rm $notableChangesSource
    fi
    
    commitHistoryOutput commits outputMsgs
}

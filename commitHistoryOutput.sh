#!/bin/bash
source './parseRepository.sh'

function commitHistoryOutput() {
    local fileOutput='CHANGELOG.md'
    local tempFile='/tmp/newfile'
    if [ -f "$tempFile" ]; then
        rm $tempFile
    fi
    
    local repositoryName=$(parseRepository)
    
    if [ -z "$1" ]; then
        echo "Error: First parameter is empty. Please send commit list"
        exit 2
        elif [ -z "$2" ]; then
        echo "Error: Second parameter is empty. Please string output array"
        exit 2
    fi
    
    param1=$1[@]
    param2=$2[@]
    commits=("${!param1}")
    outputMsgs=("${!param2}")
    
    # output
    for msg in "${outputMsgs[@]}"; do
        echo $msg >>$tempFile
    done
    
    # parse commit messages
    for commit in ${commits[@]}; do
        declare commitMessage=$(git log -1 $commit --pretty=tformat:'%B')
        ## | grep "feat:\|fix:\|docs:\|BREAKING CHANGE:\|test:\|refactor:"
        if [ ! -z "$commitMessage" ]; then
            declare shortCommitHash=$(git log -1 $commit --pretty=tformat:'%h')
            echo "-   [$shortCommitHash]($repositoryName/commits/$commit) - $commitMessage" >>$tempFile
            # echo "" >>$tempFile
        fi
    done
    echo "" >>$tempFile
    
    ## write orig file
    if [ -f "$fileOutput" ]; then
        cat $fileOutput >>$tempFile
    fi
    
    cp $tempFile $fileOutput
    
    # delete used files
    if [ -f commit-list.txt ]; then
        rm 'commit-list.txt'
    fi
    
    if [ -f commit-detail.txt ]; then
        rm 'commit-detail.txt'
    fi
}

#!/bin/bash
source './parseRepository.sh'
source './commitHistoryOutput.sh'
source './notableChangesInput.sh'

function commitHistoryUp() {
    if [ -z "$1" ]; then
        echo "Error: First parameter is empty. Please send tag parameter"
        exit 2
    elif [ -z "$2" ]; then
        echo "Error: Second parameter is empty. Please specify next tag parameter"
        exit 2
    fi

    local tag=$1
    local nextTag=$2
    outputMsgs=()
    repositoryName=$(parseRepository)
    notableChangesSource="./git-release/customChange.md"

    # if next tag exist (not new tag)
    if git tag --list | egrep -q "^$nextTag$"; then
        commits=$(git log --pretty=tformat:'%H' $tag...$nextTag)
        releaseDate=$(git log -1 --pretty='format:%ad' --date=short $nextTag)
    else
        commits=$(git rev-list $tag..HEAD)
        releaseDate=$(date +'%Y-%m-%d')
    fi

    ## notable changes
    notableChangesInput $notableChangesSource

    # head of the new release
    outputMsgs+=("## $releaseDate, version [$nextTag]($repositoryName/compare/$nextTag..$tag)")
    outputMsgs+=("")
    #outputMsgs+=("### Notable changes");

    while IFS= read -r line; do
        outputMsgs+=("$line")
    done <$notableChangesSource

    outputMsgs+=("")
    outputMsgs+=("### Commits")
    outputMsgs+=("")

    if [ -f $notableChangesSource ]; then
        rm $notableChangesSource
    fi

    commitHistoryOutput commits outputMsgs
}

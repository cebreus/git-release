#!/bin/bash
source './commitHistoryUp.sh'

function generateChangeLog() {
    if [ -f ../CHANGELOG.md ]; then
        echo "Error: CHANGELOG.md is already exist"
        exit 2
    else
        touch ../CHANGELOG.md

        declare tags=$(git tag --sort=creatordate)
        declare first=true
        declare prevTag

        for tag in $tags; do
            # first iteration
            if [ "$first" == true ]; then
                prevTag=$tag
                first=false
            # next iterations
            else
                commitHistoryUp $prevTag $tag
                echo "Changelog for tags $prevTag $tag was generated"
                prevTag=$tag
            fi
        done
    fi
}

##generateChangeLog

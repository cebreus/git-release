#!/bin/bash
function notableChangesInput() {
    if [ -z $1 ]; then
        echo "Error: mising custom MD file source"
        exit 2
    fi
    
    templateFile=$1
    
    cp "./git-release/notableChangeTemplate.md" $templateFile
    
    vim $templateFile
}

##notableChangesSource="customChange.md";
##notableChangesInput $notableChangesSource;

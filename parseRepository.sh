#!/bin/bash

function parseRepository() {
    local repository=$(git config --get remote.origin.url)
    
    if [[ $repository == *"bitbucket"* ]]; then
        local withoutPrefix=${repository#*:}
        local withoutSuffix=${withoutPrefix%.*}
        echo "https://bitbucket.org/"$withoutSuffix
    fi
    
    if [[ $repository == *"github"* ]]; then
        echo "It is github repository"
    fi
    
    # git@bitbucket.org:onio-it-team/test-chanelog.git
    # return "https://bitbucket.org/onio-it-team/test-chanelog/compare/1.3.9..1.3.8";
}

# getval=$(parseRepository);
# echo $getval;

#!/bin/bash
function release() {
    # validation
    if [ -z "$1" ]; then
        echo "Error: First parameter is empty. Please send new release version."
        exit
    fi
    
    # v1.0.0, v1.5.2, etc.
    local versionLabel=$1
    
    # establish branch and tag name variables
    local devBranch=develop
    local masterBranch=master
    local releaseBranch=release/$versionLabel

    # create the release branch from the -develop branch
    git checkout -b $releaseBranch $devBranch

    # file in which to update version number
    #versionFile="version.txt" ## todo = connect package.json?

    # find version number assignment ("= v1.5.5" for example)
    # and replace it with newly specified version number
    # remove backup file created by sed command
    ##sed -i.backup -E "s/\= v[0-9.]+/\= $versionLabel/" $versionFile $versionFile ## todo = related to todo above
    ## rm $versionFile.backup ## todo = related to todo above
    # commit version number increment
    ## git commit -am "Incrementing version number to $versionLabel" ## todo = related to todo above

    # merge release branch with the new version number into master
    git checkout $masterBranch
    git merge --no-ff --no-edit $releaseBranch
    git push

    # create tag for new version from master
    # -a = Annotated tag
    git tag $versionLabel
    git push --tags
    
    # merge release branch with the new version number back into develop
    git checkout $devBranch
    git merge --no-ff --no-edit $releaseBranch
    git push
    git push --tags

    # remove release branch
    git branch -d $releaseBranch
    
    # tag to develop
    git merge --no-ff --no-edit $masterBranch
    git push
}

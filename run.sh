#!/bin/bash
echo "Bash is only one, which is supported. Try another CLI as Git Bash, etc."

#VALIDATIONS
declare level=$1
declare changelogRange=$2

## patch validation (.bit folder)
if [ $(basename $(pwd)) != "git-release" ]; then
    echo "Error: You must be in bin folder"
    exit 1
fi

## parameters validation
if [ -z "$1" ]; then
    printf "Error: Missing sematic version type parameters.\nPlease choose one of [major, minor, patch]"
    exit 1
fi

if [ -z "$2" ]; then
    changelogRange="part"
fi

# SOURCE IMPORT
source './tagup.sh'
source './generateChangelog.sh'
source './tagup.sh'
source './release.sh'
source './commitHistoryNewTag.sh'
source './commitHistoryOutput.sh'
source './commitHistoryUp.sh'

cd ../

# TAGS DEFINITION
## last tag --abbrev=0 = (without alfa and another suffixes)
if git describe --tags >/dev/null 2>&1; then
    declare newTag=false
    declare tag=$(git describe --tags --abbrev=0)
else
    declare newTag=true
    declare tag="0.0.1"
    if [ "$changelogRange" == "full" ]; then
        echo "Error: No history finded. Please use standard release proccess."
        exit
    fi
fi

declare level=$1
declare changelogRange=$2

## next tag (major,minor,patch)
declare nextTag=$(tagup $tag $level)
if [[ $nextTag =~ "Error:" ]]; then
    echo $nextTag
    exit
fi

echo "create release for tag "$nextTag

# GENERATE CHANGELOG
if [ "$changelogRange" == "full" ]; then
    generateChangeLog
fi

## declare commitHistoryRes=$(commitHistory $tag $nextTag);
if [ "$newTag" == true ]; then
    commitHistoryNewTag $nextTag
else
    commitHistoryUp $tag $nextTag
fi

git add .
git commit -am "releaseAutomat: generate record for $nextTag..$tag"

echo "changelog generated"

# RELEASE PROCCESS
release $nextTag

echo "$(tput setaf 2)RELEASE DONE"

#!/bin/bash
function tagup() {
    local semVers=['major','minor','patch']

    # missing tag parameter
    if [ -z "$1" ]; then
        echo "Error: Missing tag parameter. Please send actula tag"
        exit 2

    # missing sem. varsion type parameter
    elif [ -z "$2" ]; then
        echo "Error: Missing sematic version type parameters. Please choose one of "
        for semVerType in ${semVers[*]}; do
            echo $semVerType
        done
        exit 2
    fi

    # sem. version validation
    if [[ ${semVers[*]} =~ "$2" ]]; then
        ## valid state
        tag=$1
        semVerType=$2
        local major="$(cut -d'.' -f1 <<<"$tag")"
        local minor="$(cut -d'.' -f2 <<<"$tag")"
        local patch="$(cut -d'.' -f3 <<<"$tag")"

        case $semVerType in
        "major")
            major=$((major + 1))
            minor=0
            patch=0
            ;;
        "minor")
            minor=$((minor + 1))
            patch=0
            ;;
        "patch") patch=$((patch + 1)) ;;
        esac

        echo $major.$minor.$patch
    else
        echo "Error: Bad sematic level type. Please choose one of "
        for semVerType in "${semVers[*]}"; do
            echo $semVerType
        done
        exit 2
    fi
}

#tag='1.0.2';
#level="patch";
#getval=$(tagup $tag $level);
#echo $getval;

#major: Incompatible API changes were introduced
#minor: Functionality was added in a backwards-compatible manner
#patch: Backwards-compatible bug fixes were applied
